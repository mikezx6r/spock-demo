package demo.spock
import demo.spock.form.AddressForm
import demo.spock.model.Address
import demo.spock.repository.AddressRepository
import demo.spock.service.PowerfulService
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import spock.lang.Specification
/**
 * Shows some of the advanced stubbing features that generally can be performed in other frameworks, but
 * require more coding
 *
 * @author Mike Wilkes
 */
class AdvancedStubbingSpec extends Specification {

    private static final int ADDRESS_ID = 12

    PowerfulService service = new PowerfulService()
    Validator validator = Stub()
    AddressRepository addressRepository = Mock() {
        // using map of field username to value in constructor to construct Address object
        get(ADDRESS_ID) >> new Address([id:ADDRESS_ID, streetName:'some st'])
    }

    def setup() {
        service.addressRepository = addressRepository
        service.validator = validator
    }

    def "object should not be stored if validator generates an error"() {
        setup: "When validate method is called, add a reject message to the second argument (Errors in this case)."
        validator.validate(_, _ as Errors) >> {args -> args[1].reject("some error") }

        when:
        service.store(ADDRESS_ID, new AddressForm());

        then: "as there are errors, we expect that saveOrUpdate will not be called"
        0 * addressRepository.saveOrUpdate(_)
    }

    def "object that validates should have CA set as its country"() {
        when: "store an Address form"
        service.store(ADDRESS_ID, new AddressForm());

        then: "Expect saveOrUpdate to be called with an Address that has country of Canada."
        1 * addressRepository.saveOrUpdate({it.country == 'CA'})
    }
}