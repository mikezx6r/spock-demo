package demo.spock

import demo.spock.interaction.Subscriber
import demo.spock.model.Address
import spock.lang.Specification
import spock.lang.Unroll


/**
 * Demonstrate Spock's powerful mocking and stubbing capabilities
 *
 * author Mike Wilkes
 */
class InteractionSpec extends Specification {

	// Spock differentiates between Mocks and Stubs
	Subscriber subStub = Stub()
	Subscriber subMock = Mock()

	def "Using a stub, methods return reasonable default values, or empty Objects of correct type"() {
		when:
		def address = subStub.getAddress()

		then: "using Groovy truth, verify that stub returns non-null object"
		address

		and: "the contained field is null"
		!address.streetName
	}

	def "Using a mock, methods return null for Object return types"() {
		when:
		def address = subMock.getAddress()

		then: "because we're using a mock, address will be null (not Groovy true)"
		!address
	}

	def "Using a mock, and returning a value"() {
		setup: "unless we define a return value (Note use of Groovy named properties in constructor"
		subMock.getAddress() >> new Address(["streetName": "Yonge St"])

		when:
		def address = subMock.getAddress()

		then:
		address
		address.streetName == "Yonge St"
	}

	def "Using a mock, and verifying all values"() {
		setup: "Note that the mock will return this value everytime it's called"
		subMock.getAddress() >> new Address(["streetNumber": "135", "streetName": "Bloor St", "province": "ON", "postCode": "A1A1A1"])

		when:
		def address = subMock.getAddress()

		then:
		address

		and: "validate a number of fields on address without having to explicitly repeat address."
		with(address) {
			streetNumber == "135"
			streetName == "Bloor St"
			postCode == "A1A1A1"
		}

		when: "call the same method again"
		def address2 = subMock.getAddress()

		then: "get the same result"
		address2
		with(address2) {
			streetNumber == "135"
			streetName == "Bloor St"
			postCode == "A1A1A1"
		}
	}

	def "Define more than one result value"() {
		setup:
		subStub.payBill(_, _) >>> [true, false, false, true]

		when: "returns first result in array"
		def result = subStub.payBill(new Date(), 3.5g)
		then:
		result

		when: "and second"
		result = subStub.payBill(new Date(), 4.5g)
		then:
		!result

		when: "third"
		result = subStub.payBill(new Date(), 4.5g)
		then:
		!result

		when: "fourth"
		result = subStub.payBill(new Date(), 4.5g)
		then:
		result

		when: "any further interactions will return the last value"
		result = subStub.payBill(new Date(), 4.5g)
		then:
		result
	}

	def "Explicitly control number of interactions on a mock. Define to only accept 2 calls to receiveNewspaper, but make 3"() {
		when:
		(1..3).each { subMock.receiveNewspaper() }

		then:
		2 * subMock.receiveNewspaper()
	}

	def "Explicitly control interactions, and don't allow any additional interactions"() {
		when:
		(1..2).each { subMock.receiveNewspaper() }

		and: "Expect this to fail as haven't defined interaction, and enforcing 'no additional interactions'"
		subMock.address

		then:
		2 * subMock.receiveNewspaper()
		0 * _
	}

	def "Allow any number of interactions"() {
		when:
		(1..5).each { subMock.receiveNewspaper() }

		then:
		_ * subMock.receiveNewspaper()
	}

	@Unroll
	def "based on arguments passed, return a result. Use named parameters:(#date, #amount) - #result"() {
		setup: "If bill payment greater than 100, return true, otherwise return false"
		subMock.payBill(_ as Date, _ as BigDecimal) >> { date, amount -> amount > 100 }

		expect:
		subMock.payBill(date, amount) == result

		where:
		date       | amount || result
		new Date() | 101    || true
		new Date() | 100    || false
		new Date() | 99     || false
	}

	def "also define interactions using explicit variables"() {
		setup:
		def date = new Date()
		subStub.payBill(date, _) >> true

		when: "matching interaction because date is expected date parameter"
		def result = subStub.payBill(date, 100)
		then: "therefore stub will return true"
		result

		when: "when date parameter doesn't match, falls back to default interaction"
		result = subStub.payBill(new Date(), 100)

		then: "which should return false"
		!result
	}

	// try changing the order of the thens. The test will fail if amount = 400 interaction is first
	def "can also define order of interactions"() {
		setup:
		def date = new Date()
		when:
		subMock.payBill(date, 200)
		subMock.payBill(date, 400)

		then: "define interactions"
		1 * subMock.payBill(_, 200)

		then: "and because this is in a separate 'then', the previous then must complete before this one is 'active'"
		1 * subMock.payBill(_, 400)
	}
}