package demo.spock

import spock.lang.Specification


/**
 * Demonstrate basic exception handling in Spock
 *
 * @author Mike Wilkes
 */
class ExceptionSpec extends Specification {
	def "expect an exception"() {
		setup:
		def a = null

		when:
		a.pop()

		then:
		NullPointerException e = thrown()
		e.message.contains("method pop()")
	}

	def "explicitly define that an exception should NOT be thrown"() {
		setup: "Create an empty map"
		def map = new HashMap()

		when: "add an item with a null key"
		map.put(null, "some value")

		then: "it should be accepted"
		notThrown(NullPointerException)
	}
}