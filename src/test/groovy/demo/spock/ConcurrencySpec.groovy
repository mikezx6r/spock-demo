package demo.spock

import spock.lang.Specification
import spock.util.concurrent.PollingConditions
/**
 * This spec demonstrates testing of concurrent actions.
 */
class ConcurrencySpec extends Specification {


	def "concurrent testing using PollingConditions"() {
		given: "a person object"
		def person = new Person(name: "Fred", age: 22)

		and: "polling conditions with the 1s default changed to 10s"
		def conditions = new PollingConditions(timeout: 10)

		when: "perform an action in a thread with delays"
		Thread.start {
			sleep(1000)
			person.age = 42
			sleep(5000)
			person.name = "Barney"
		}
		then: "ensure condition is satisfied within 2 seconds"
		conditions.within(2) { assert person.age == 42 }

		and: "ensure it is satisfied eventually. In this case, it will wait up to 10s as the timeout was changed in the given:."
		conditions.eventually { assert person.name == "Barney" }

	}
}

class Person {
	String name;
	Integer age;
}