package demo.spock

import demo.spock.model.Account
import demo.spock.model.Franchise
import demo.spock.repository.AccountRepository
import demo.spock.service.AccountService
import org.joda.time.DateTime
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Demonstrate testing complex flow logic. Start by showing the process by creating indiviual test methods, and then
 * how easy Spock makes it to use data-driven method instead.
 */
class CompleteVerificationUsingDataDrivenSpec extends Specification {

    final static Long FRANCHISE_ID = 12L
    final static Long USER_ID = 200L
    private static final String USER_NAME = "dbAccount username"
    private static final String PASSWORD = "PASSWORD123"

    def accountService = new AccountService()
    AccountRepository accountRepository = Stub()

    def setup() {
        accountService.accountRepository = accountRepository
    }

    def "A user should be allowed to login for an active franchise"() {
        given: "an active franchise"
        Franchise franchise = new Franchise([id: FRANCHISE_ID, endDate: new DateTime().plusMonths(1).toDate()])
        and: "a validated dbAccount for this company"
        Account dbAccount = new Account([id: USER_ID, franchiseId: franchise.id, username: USER_NAME, password: PASSWORD])
        dbAccount.validated = true
        and: "database responses"
        accountRepository.getFranchise(FRANCHISE_ID) >> franchise
        accountRepository.getAccount(USER_NAME, PASSWORD) >> dbAccount

        when: "this user logs in"
        Account account = accountService.login(franchise.id, USER_NAME, PASSWORD)

        then: "the account should proceed"
        account.proceed
    }

    def "A user should not be allowed to login for an active franchise, if he is not validated yet"() {
        given: "an active franchise"
        Franchise franchise = new Franchise([id: FRANCHISE_ID, endDate: new DateTime().plusMonths(1).toDate()])
        and: "a not yet validated user for this company"
        Account dbAccount = new Account([id: USER_ID, franchiseId: franchise.id, username: USER_NAME, password: PASSWORD])
        dbAccount.validated = false

        when: "this user logs in"
        Account account = accountService.login(franchise.id, USER_NAME, PASSWORD)

        then: "the user's account should be access denied"
        !account.proceed
    }

    // and so on
    // and so on to cover all variations

    // Or test all cases in one method
    @Unroll
    def "should test all the relations for a valid or non valid login: #explanation"() {
        def franchise = new Franchise()
        def dbAccount = new Account()
        setup: "stub DB return values"
        accountRepository.getFranchise(franchiseId) >> franchise
        accountRepository.getAccount(USER_NAME, PASSWORD) >> dbAccount

        expect: "based on data from table, setup objects"
        franchise.setId(franchiseId)
        franchise.setEndDate(date.toDate())

        dbAccount.setId(USER_ID)
        dbAccount.setFranchiseId(franchiseId)
        dbAccount.setUsername(username)
        dbAccount.setPassword(password)
        dbAccount.setValidated(validated)

        and: "user logs in"
        Account account = accountService.login(franchiseId, username, password)

        and: "verify we got expected result"
        account.proceed == result

        where:
        explanation              | franchiseId | date                          | validated | username  | password      || result
        "valid login"            | 1           | new DateTime().plusMonths(2)  | true      | USER_NAME | PASSWORD      || true
        "not part of franchise"  | 2           | new DateTime()                | true      | USER_NAME | PASSWORD      || false
        "franchise has ended"    | 1           | new DateTime().minusMonths(2) | true      | USER_NAME | PASSWORD      || false
        "user not validated yet" | 1           | new DateTime()                | false     | USER_NAME | PASSWORD      || false
        "invalid user"           | 1           | new DateTime()                | true      | "you"     | PASSWORD      || false
        "invalid password"       | 1           | new DateTime()                | true      | USER_NAME | "badpassword" || false
    }
}