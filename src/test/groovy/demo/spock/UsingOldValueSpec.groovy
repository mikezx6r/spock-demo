package demo.spock

import spock.lang.Specification


/**
 * Demonstrate usages of old
 *
 * @author Mike Wilkes
 */
class UsingOldValueSpec extends Specification {

	def "simple example"() {
		setup:
		def builder = new StringBuilder("start + 1")

		when:
		builder << "value"

		then: "compare old value + new value"
		builder.toString() == old(builder.toString()) + "value"

		when: "add another value"
		builder << "another value"

		then: "and notice that old returns the value from the most recent when."
		"start + 1value" == old(builder.toString())

		builder.toString() == old(builder.toString()) + "another value"
	}
}