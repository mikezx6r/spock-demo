package demo.spock

import demo.spock.service.OtherService
import demo.spock.service.PowerfulService
import spock.lang.Specification
import spock.lang.Unroll


/**
 * Demonstrate testing a class with private methods
 *
 * @author Mike Wilkes
 */
class TestingAClassSpec extends Specification {

	def fixture = new PowerfulService()

	@Unroll
   def "Can test private methods w/o having to change their access"() {
	   expect:
	   result == fixture.someInternalMethod(name, number)

	   where:
	   name|number||result
	   "somename"|12||"12"
	   "someappendName"|13 ||"someappendName13"
   }

	def "can inject a collaborator directly. No need to add an extra library (e.g. spring-test"() {
		setup:
		// define Stub and its methods all in one statement
		OtherService otherStub = Stub() {
			// accept any value as parameter, and return it (i.e. first parameter)
			someHelperMethod(_) >> {it[0]}
		}

		// set value of interface right on fixture w/o modifying it
		fixture.otherService = otherStub

		when:
		def result = fixture.usesInjectedServer("some parameter")

		then:
		result == "some parameter"
	}
}