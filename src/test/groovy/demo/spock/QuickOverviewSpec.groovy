package demo.spock

import spock.lang.Specification
import spock.lang.Unroll

/**
 * Simple demonstration of various styles within Spock
 *
 * @author Mike Wilkes
 */
class QuickOverviewSpec extends Specification {
	def setup() {
		// do whatever setup is required before every feature method
	}

	def cleanup() {
		// do whatever cleanup is required after every feature method
	}

	def setupSpec() {
		// do whatever setup is required before running the entire spec.
	}

	def cleanupSpec() {
		// do whatever cleanup is required after running the entire spec
	}

	def "a simple test showing expect. Expect is generally clearer for functional methods or used for data-driven tests."() {
		expect:
		Math.max(1, 6) == 6
	}

	def "another simple test that demonstrates when/then. This format will be for most interactions"() {
		when:
		def max = Math.max(1, 6)

		then:
		max == 6
	}

	@Unroll
	def "data-driven test using arrays"() {
		expect:
		Math.max(a, b) == c

		where:
		a << [3, 5, 9]
		b << [7, 4, 9]
		c << [5, 5, 9]
	}

	// When this test fails, it will be hard to see which row caused the failure.
	// Uncomment the @Unroll to see the difference
	// @Unroll
	def "data-driven test using 'table' #a, #b is #c. Expect error in 1 row."() {
		expect:
		Math.max(a, b) == c

		where:
		a | b || c
		3 | 7 || 3
		5 | 4 || 5
		9 | 9 || 9
	}

	def "a method that shows all the Spock lifecycle stages"() {
		setup: "get everything configured and constructed for the test"
		def aString = "something"

		when: "perform the actual actual that should be tested"
		aString = aString.toUpperCase()

		then: "and validate the results"
		aString  // this is using Groovy Truth to ensure aString is not null
		and: "can validate additional results either inline, or with and: statements to make it read better"
		aString == "SOMEtHING"

		cleanup: "and finally, cleanup after the test"
		aString = null
	}
}
