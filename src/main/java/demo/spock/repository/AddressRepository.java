package demo.spock.repository;

import demo.spock.model.Address;

/**
 * {Enter a description here}
 *
 * @author Mike Wilkes
 */
public interface AddressRepository {
	Address get(Long id);

	void saveOrUpdate(Address persistedObject);
}
