package demo.spock.repository;

import demo.spock.model.Account;
import demo.spock.model.Franchise;

public interface AccountRepository {
    Franchise getFranchise(long franchiseId);

    Account getAccount(String username, String password);
}
