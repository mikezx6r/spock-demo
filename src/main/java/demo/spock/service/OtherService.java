package demo.spock.service;

/**
 * Interface used to demonstrate DI mocking
 *
 * @author Mike Wilkes
 */
public interface OtherService {
	String someHelperMethod(String parm);
}
