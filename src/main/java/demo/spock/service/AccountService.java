package demo.spock.service;

import demo.spock.model.Account;
import demo.spock.model.Franchise;
import demo.spock.repository.AccountRepository;

import java.util.Date;

/**
 * User: Mike
 */
public class AccountService {
    private AccountRepository accountRepository;

    public Account login(long franchiseId, String username, String password) {
        Franchise f = accountRepository.getFranchise(franchiseId);
        Account a = accountRepository.getAccount(username, password);
        if(!a.isValidated()) {
            a.setProceed(false);
        } else if(new Date().after(f.getEndDate())) {
            a.setProceed(false);
        } else if(a.getFranchiseId() != f.getId()) {
            a.setProceed(false);
        } else {
            a.setProceed(username.equals(a.getUsername()) && password.equals(a.getPassword()));
        }
        return a;
    }
}
