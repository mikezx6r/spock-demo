package demo.spock.service;

import demo.spock.form.AddressForm;
import demo.spock.model.Address;
import demo.spock.repository.AddressRepository;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.Validator;

/**
 * A powerful service used to demonstrate testing in Spock
 *
 * @author Mike Wilkes
 */
public class PowerfulService {

	private OtherService otherService;
	private AddressRepository addressRepository;
	private Validator validator;

	private String someInternalMethod(String name, Integer number) {
		if (name.toLowerCase().contains("append")) {
			return name + number;
		}
		return number.toString();
	}

	public String usesInjectedServer(String parm) {
		return otherService.someHelperMethod(parm);
	}

	public void store(Long id, AddressForm form) {
		Address persistedObject = addressRepository.get(id);
		persistedObject.merge(form);
        Errors errors = new BeanPropertyBindingResult(persistedObject, "address");
        validator.validate(persistedObject, errors);
		if (!errors.hasErrors()) {
            persistedObject.setCountry("CA");
            addressRepository.saveOrUpdate(persistedObject);
        }
    }
}
