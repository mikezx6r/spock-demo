package demo.spock.interaction;

import demo.spock.model.Address;

import java.math.BigDecimal;
import java.util.Date;

/**
 * An interface to demonstrate Spock's stubbing and mocking capabilities
 *
 * @author Mike Wilkes
 */
public interface Subscriber {
	void receiveNewspaper();

	boolean payBill(Date paymentDate, BigDecimal paymentAmount);
	Address getAddress();
}
